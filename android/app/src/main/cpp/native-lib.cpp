#include <jni.h>

#include <android/log.h>

#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <sys/socket.h>

#include <pthread.h>

#define PORT 11826
#define MAXMSG 512

JavaVM* jvm;
long sock = 0;
jobject main_activity = NULL;

struct context
{
    long sock = 0;
    JNIEnv *env = NULL;
};

void
displayMessage(struct context ctx, const char* msg)
{
    JNIEnv* env = ctx.env;

    jstring jmsg = env->NewStringUTF(msg);

    jclass clazz = env->GetObjectClass(main_activity);
    jmethodID mid = env->GetMethodID(clazz, "receiveMessage", "(Ljava/lang/String;)V");

    if(mid == nullptr)
        abort();

    env->CallVoidMethod(main_activity, mid, jmsg);

}

[[noreturn]] void
read_from_server(struct context ctx)
{
    char buff[MAXMSG];

    while(1)
    {
        bzero(buff, sizeof(buff));
        read(ctx.sock, buff, sizeof(buff));
        displayMessage(ctx, buff);
    }
}

void*
read_from_server_(void* ctx_)
{
    struct context* ctx = (struct context*)ctx_;
    read_from_server(*ctx);
}

extern "C"
JNIEXPORT void JNICALL
Java_org_ketabuk_zy_MainActivity_writeToServer(JNIEnv *env, jobject thiz, jstring msg) {
    const char *c_msg = env->GetStringUTFChars(msg, NULL);
    write(sock, c_msg, strlen(c_msg));
}

extern "C"
JNIEXPORT void JNICALL
Java_org_ketabuk_zy_MainActivity_connectToServer(JNIEnv *env, jobject thiz) {
    struct sockaddr_in servaddr, cli;

    // socket create and verification
    sock = socket(AF_INET, SOCK_STREAM, 0);
    if (sock < 0)
    {
        perror("socket");
        __android_log_write(ANDROID_LOG_ERROR, "Fatal", strerror(errno));
        abort();
    }

    bzero(&servaddr, sizeof(servaddr));

    // assign IP, PORT
    servaddr.sin_family = AF_INET;
    servaddr.sin_addr.s_addr = inet_addr("167.71.65.161");
    servaddr.sin_port = htons(PORT);

    // connect the client socket to server socket
    if (connect(sock, (struct sockaddr*)&servaddr, sizeof(servaddr)) != 0) {
        printf("connection with the server failed...\n");
        exit(0);
    }
    else
        printf("connected to the server..\n");
}

extern "C"
JNIEXPORT void JNICALL
Java_org_ketabuk_zy_MainActivity_closeConnection(JNIEnv *env, jobject thiz)
{
    // close the socket
    close(sock);
}

extern "C"
JNIEXPORT void JNICALL
Java_org_ketabuk_zy_MainActivity_initC(JNIEnv *env, jobject thiz) {
    env->GetJavaVM(&jvm);
    main_activity = reinterpret_cast<jobject>(env->NewGlobalRef(thiz));
}

extern "C"
JNIEXPORT void JNICALL
Java_org_ketabuk_zy_MainActivity_readFromServer(JNIEnv *env, jobject thiz) {
    struct context ctx;
    ctx.sock = sock;
    ctx.env = env;
    read_from_server(ctx);
}