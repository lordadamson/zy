#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <sys/socket.h>

#include <pthread.h>

#define PORT 11826
#define MAXMSG 512

void
write_to_server(int sockfd)
{
    char buff[MAXMSG];
    int n;

    while(1)
    {
        bzero(buff, sizeof(buff));
        printf("Enter message: ");
        fflush(stdout);
        n = 0;

        while ((buff[n++] = getchar()) != '\n');

        write(sockfd, buff, sizeof(buff));
        bzero(buff, sizeof(buff));

        if ((strncmp(buff, "exit", 4)) == 0)
        {
            printf("Client Exit...\n");
            break;
        }
    }
}

void*
write_to_server_(void* sockfd)
{
    write_to_server((long)sockfd);
    return NULL;
}

void*
read_from_server(int sockfd)
{
    char buff[MAXMSG];
    int n;

    while(1)
    {
        bzero(buff, sizeof(buff));
        read(sockfd, buff, sizeof(buff));
        printf("From Server: %s\n Enter message: ", buff);
        fflush(stdout);

        if ((strncmp(buff, "exit", 4)) == 0)
        {
            printf("Client Exit...\n");
            break;
        }
    }

    return NULL;
}

void*
read_from_server_(void* sockfd)
{
    read_from_server((long)sockfd);
    return NULL;
}

int main()
{
    struct sockaddr_in servaddr, cli;

    // socket create and varification
    long sock = socket(AF_INET, SOCK_STREAM, 0);
    if (sock < 0)
    {
        perror("socket");
        abort();
    }

    bzero(&servaddr, sizeof(servaddr));

    // assign IP, PORT
    servaddr.sin_family = AF_INET;
    servaddr.sin_addr.s_addr = inet_addr("167.71.65.161");
    servaddr.sin_port = htons(PORT);

    // connect the client socket to server socket
    if (connect(sock, (struct sockaddr*)&servaddr, sizeof(servaddr)) != 0) {
        printf("connection with the server failed...\n");
        exit(0);
    }
    else
        printf("connected to the server..\n");

    pthread_t t;
    pthread_create(&t, NULL, write_to_server_, (void*)sock);

    read_from_server(sock);

    pthread_join(t, NULL);

    // close the socket
    close(sock);
}
