#include <unistd.h>
#include <arpa/inet.h>
#include <sys/select.h>
#include <sys/socket.h>
#include <netinet/in.h>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define PORT 11826
#define MAXMSG 512

static int
make_socket(uint16_t port)
{
    int sock;
    struct sockaddr_in name;

    /* Create the socket. */
    sock = socket(PF_INET, SOCK_STREAM, 0);
    if (sock < 0)
    {
        perror("socket");
        abort();
    }

    memset(&name, 0, sizeof(name));

    /* Give the socket a name. */
    name.sin_family = AF_INET;
    name.sin_port = htons(port);
    name.sin_addr.s_addr = htonl(INADDR_ANY);

    if(bind(sock, (struct sockaddr *) &name, sizeof(name)) < 0)
    {
        perror("bind");
        abort();
    }

    return sock;
}

#define MAX_CONNECTED 50

static int connected_users[MAX_CONNECTED] = {0};

fd_set active_fd_set, read_fd_set;

static void
erase_client(int clientfd)
{
    for(size_t i = 0; i < MAX_CONNECTED; i++)
    {
        if(connected_users[i] == clientfd)
        {
            connected_users[i] = 0;
        }
    }

    close(clientfd);
    FD_CLR(clientfd, &active_fd_set);
    printf("client disconnected\n");
}

static int
add_client(int clientfd)
{
    for(size_t i = 0; i < MAX_CONNECTED; i++)
    {
        if(connected_users[i] == 0)
        {
            connected_users[i] = clientfd;
            return 0;
        }
    }

    return -1;
}

static void
broadcast(const char* msg, size_t size, int from_fd)
{
    for(size_t i = 0; i < MAX_CONNECTED; i++)
    {
        if(connected_users[i] != 0)
        {
            if(write(connected_users[i], msg, size) < 0)
            {
                erase_client(i);
            }
        }
    }
}

static int
read_from_client (int clientfd)
{
    char buffer[MAXMSG];

    int nbytes = read(clientfd, buffer, MAXMSG);

    if (nbytes < 0)
    {
        /* Read error. */
        return -1;
    }
    else if (nbytes == 0)
    {
        /* End-of-file. */
        return -1;
    }
    else
    {
        /* Data read. */
        fprintf (stderr, "Server: got message: `%s'\n", buffer);
        broadcast(buffer, nbytes, clientfd);
        return 0;
    }
}

int
main()
{
    int sock = make_socket(PORT);

    if(listen(sock, 1) < 0)
    {
        perror ("listen");
        abort();
    }


    struct sockaddr_in clientname;

    memset(&active_fd_set, 0, sizeof(active_fd_set));
    FD_SET (sock, &active_fd_set);

    while(1)
    {
        read_fd_set = active_fd_set;

        if(select(FD_SETSIZE, &read_fd_set, NULL, NULL, NULL) < 0)
        {
            perror("select");
            abort();
        }

        for(size_t i = 0; i < FD_SETSIZE; ++i)
        {
            if(!FD_ISSET(i, &read_fd_set))
            {
                continue;
            }

            if (i == sock)
            {
                /* Connection request on original socket. */
                int new;
                socklen_t size = sizeof(clientname);
                new = accept(sock,
                             (struct sockaddr *) &clientname,
                             &size);

                if (new < 0)
                {
                    perror("accept");
                    abort();
                }

                fprintf (stderr,
                         "Server: connect from host %s, port %hu.\n",
                         inet_ntoa(clientname.sin_addr),
                         ntohs(clientname.sin_port));

                if(add_client(new) != 0)
                {
                    close(new);
                    continue;
                }

                FD_SET(new, &active_fd_set);
            }
            else
            {
                /* Data arriving on an already-connected socket. */
                if(read_from_client(i) < 0)
                {
                    erase_client(i);
                }
            }
        }
    }
}
