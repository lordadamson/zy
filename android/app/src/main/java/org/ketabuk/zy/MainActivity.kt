package org.ketabuk.zy

import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.View
import android.widget.EditText
import android.widget.ScrollView
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import kotlin.concurrent.thread


class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        username = intent.getStringExtra(EXTRA_MESSAGE)!!

        // Example of a call to a native method
        //findViewById<TextView>(R.id.chat_log).text = stringFromJNI()
        connectToServer()
        initC()

        thread {
            readFromServer()
        }
    }

    private var username: String = ""

    fun onSendBtnClicked(c: View) {
        var msg_box = findViewById(R.id.msg_box) as EditText
        var msg = msg_box.text.toString()
        writeToServer("$username: $msg")
        msg_box.text.clear()
    }

    override fun onDestroy() {
        super.onDestroy()
        closeConnection()
    }

    fun receiveMessage(msg: String) {
        var chat_log = findViewById(R.id.chat_log) as TextView
        var scroll_view = findViewById(R.id.text_scrollview) as ScrollView

        Handler(Looper.getMainLooper()).post(Runnable {
            chat_log.text = chat_log.text.toString() + "\n" + msg
            scroll_view.postDelayed({scroll_view.scrollTo(0, chat_log.height)}, 100)
        })
    }

    private external fun initC()
    private external fun connectToServer()

    private external fun closeConnection()

    private external fun readFromServer()
    private external fun writeToServer(msg: String)

    companion object {
        // Used to load the 'native-lib' library on application startup.
        init {
            System.loadLibrary("native-lib")
        }
    }
}